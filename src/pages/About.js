import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

const About = () => {
    return (
        <div>
            <Logo />
            <Navigation />
            <h1>A propos</h1>
            <br />
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus dolorum praesentium facere provident
                minus ipsam veniam eaque officia neque dolor dolore ut tempore soluta error, eum libero nobis sed accusantium ea,
                assumenda quos velit numquam quae accusamus. Nesciunt ipsam praesentium explicabo, molestias assumenda maxime eius numquam?
                Esse necessitatibus, provident deleniti ratione illum sit quae soluta, numquam reiciendis qui ea ipsum, aliquid accusantium
                neque reprehenderit saepe! Accusamus assumenda fugiat delectus voluptatum incidunt nulla, expedita, perspiciatis numquam
                quod dolorum deserunt porro laboriosam omnis nihil in, earum molestiae? Culpa, enim? Veniam, aut asperiores reiciendis
                numquam, alias aliquid esse molestias ipsam necessitatibus beatae odio.</p>
            <br />
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusamus nulla corporis expedita! Libero debitis
                aspernatur eos, accusamus quasi, voluptas illo reprehenderit ut odit sunt, harum ipsa facere cum sequi ipsum dicta itaque.
                Fugit vitae accusantium cum illum dolore earum officiis placeat quidem mollitia repellendus accusamus maiores aut assumenda
                hic ducimus consectetur totam quod, molestias iure minus adipisci necessitatibus! Tenetur, modi suscipit! Quod, vitae.
                Minus explicabo quasi repellat aut non sunt dicta nulla expedita quas. Nemo consequatur temporibus sunt voluptatibus,
                ullam inventore ab similique ratione commodi accusamus eveniet ad voluptas natus nam modi placeat atque voluptates
                consequuntur unde alias officia aliquam, praesentium eligendi. Nemo repudiandae sequi veritatis quis amet provident,
                vel ipsam mollitia, consequatur distinctio asperiores. Eveniet dolores iste laborum placeat eaque adipisci maiores
                aperiam repudiandae quas aliquam, magnam eos eligendi ex in facere doloribus, rerum ullam odio quasi quam. Molestias,
                qui repudiandae similique voluptatem maxime excepturi cum laudantium fuga, aliquam voluptatum animi dolorum reprehenderit.
                Qui saepe rerum magni, adipisci necessitatibus eligendi! A ab dolorem consequuntur maxime quae vel alias, necessitatibus
                expedita aliquid impedit corporis quidem illo odit veniam minus nemo! Fuga tempora porro accusantium perferendis maiores
                veniam ipsam voluptatibus iusto. Pariatur nihil voluptatem aut velit, aperiam dolore officiis debitis amet!</p>
        </div>
    );
};

export default About;